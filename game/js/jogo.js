var timerId = null; // Armazena chamada da função 

function iniciaJogo() {
	var url = window.location.search;
	var nivelJogo = url.replace("?","");

	var tempoSegundos = 0;

	if (nivelJogo == 'easy') {
		tempoSegundos = 120;
	}
	if (nivelJogo == 'medium') {
		tempoSegundos = 60;
	}	
	if (nivelJogo == 'hard') {
		tempoSegundos = 30;
	}

	document.getElementById('tempoCronometro').innerHTML = tempoSegundos;

	var qtdBaloes = 20;

	criaBaloes(qtdBaloes);
	document.getElementById('baloesInteiros').innerHTML = qtdBaloes;
	document.getElementById('baloesEstourados').innerHTML = 0;

	contagemTempo(tempoSegundos + 1);
}

function gameOver() {
	removeEventosBaloes();
	alert('Fim de jogo, você não conseguiu!')
}

function contagemTempo(segundos) {
	segundos = segundos - 1;
	if (segundos == -1) {
		clearTimeout(timerId); //Para a execução da função setTimeout
		gameOver();
		return false;
	}
	document.getElementById("tempoCronometro").innerHTML = segundos;
	timerId = setTimeout("contagemTempo("+ segundos +")",1000)
}


function criaBaloes(qtdBaloes) {
	for (var i = 1; i <= qtdBaloes; i++) {
		var balao = document.createElement("img");
		balao.src = 'imagens/balao_azul_pequeno.png';
		balao.style.margin = '10px';
		balao.id = "b"+i;
		balao.onclick = function() { estourar(this); }
		document.getElementById('cenario').appendChild(balao);
	}
}

function estourar(e) {	
	var idBalao = e.id;
	document.getElementById(idBalao).setAttribute("onclick","")
	document.getElementById(idBalao).src = 'imagens/balao_azul_pequeno_estourado.png';

	pontucao(-1);
}

function pontucao(acao) {
	var baloesInteiros = document.getElementById("baloesInteiros").innerHTML
	var baloesEstourados = document.getElementById("baloesEstourados").innerHTML

	baloesInteiros = parseInt(baloesInteiros);
	baloesEstourados = parseInt(baloesEstourados);

	baloesInteiros = baloesInteiros + acao;
	baloesEstourados = baloesEstourados - acao;

	document.getElementById('baloesInteiros').innerHTML = baloesInteiros;
	document.getElementById('baloesEstourados').innerHTML = baloesEstourados;

	situacaoJogo(baloesInteiros);

}

function situacaoJogo(baloesInteiros) {
	if(baloesInteiros == 0)	{
		alert('Parabens, você conseguiu!')
		pararJogo();
	}
}


function pararJogo() {
	clearTimeout(timerId);
}

function removeEventosBaloes() {
    var i = 1; //contado para recuperar balões por id

    //percorre o lementos de acordo com o id e só irá sair do laço quando não houver correspondência com elemento
    while(document.getElementById('b'+i)) {
        //retira o evento onclick do elemento
        document.getElementById('b'+i).onclick = '';
        i++; //faz a iteração da variávei i
    }
}